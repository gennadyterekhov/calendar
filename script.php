<?php

require_once('autoload.php');
require_once('vendor/autoload.php');


function getConfigVars () {
    $config_json = json_decode(
        file_get_contents(
            dirname(__FILE__) . '/.env.json'
        ),
        $assoc = true
    );
    return $config_json;
}


function validateId ($id) {
    return ( 1 === preg_match( '~^[1-9][0-9]*$~', $id ));
}


function validateGetParams () {
    foreach ($_GET as $key => $val) {
        if (!validateId($val)) {
            echo 'FATAL invalid params: expect numeric IDs';
            die();
        }
    }
    return true;
}


function getGetParams () {
    $dateFieldId = 0;
    $statusesToSearch = [];

    if (isset($_GET['date_field_id'])) {
        foreach ($_GET as $key => $val) {
            if ($key === 'date_field_id') {
                $dateFieldId = $_GET['date_field_id'];
            } else {
                $statusesToSearch[] = intval($val);
            }
        }
    } else {
        echo 'FATAL invalid params: expect date_field_id as 1st param';
        die();       
    }
    return ['date_field_id' => intval($dateFieldId), 'statuses' =>$statusesToSearch];
} 


function isCustomFieldSet ($lead, $customFieldId, $value) {
    foreach ($lead['custom_fields'] as $customField) {
        if ($customField['id'] === $customFieldId) {
            if ($customField['values'][0]['value'] === $value) {
                return true;
            }
            return false;
        }
    }
    return false;
}


function leadsHaveCustomFieldSet ($leads, $customFieldId, $value) {
    $leadsBooked = [];
    foreach ($leads as $el) {
        if (isCustomFieldSet($el, $customFieldId, $value)) {
            array_push($leadsBooked, $el);
        }
    }
    return $leadsBooked;
}


function leadsWithCustomFieldCount ($leads, $customFieldId, $value) {
    $leadsBooked = leadsHaveCustomFieldSet($leads, $customFieldId, $value);
    return count($leadsBooked);
}


function isCustomField ($lead, $customFieldId) {
    foreach ($lead['custom_fields'] as $customField) {
        if ($customField['id'] === $customFieldId) {
            return true;
        }
    }
    return false;
}


function leadsHaveCustomField ($leads, $customFieldId) {
    $leadsBooked = [];
    foreach ($leads as $el) {
        if (isCustomField($el, $customFieldId)) {
            array_push($leadsBooked, $el);
        }
    }
    return $leadsBooked;
}


function getAllLeadsChunked ($api, $crm_user_id = [], $status = [], $id = [], $ifmodif = '', $count = 0) {
    $count = getConfigVars()['chunk_size'];
    $allLeads = [];
    $offset = 0;
    while (true) {
        $result = $api->lead->getAll($crm_user_id, $status, $id, $ifmodif, $count, $offset);
        $allLeads = array_merge($allLeads, $result['result']);
        $resCount = count($result['result']);
        
        if ($resCount == $count) {
            $offset += $count;
        } else {
            return $allLeads;
        }
    }
}


function getBooksCountForEachDayInMonth ($api) {
    $leadsInStatus = getAllLeadsChunked($api, $crm_user_id = [], $status = getGetParams()['statuses'], $id = [], $ifmodif = '');
    $leadsWithDate = leadsHaveCustomField($leads = $leadsInStatus, $customFieldId = getGetParams()['date_field_id']);

    $dates = [];
    $books = [];
    $secondsInADay = 60 * 60 * 24;
    for ($i = 0; $i < 30; $i += 1) {
        $dates[$i] = date('Y-m-d 00:00:00', time() + $i * $secondsInADay);
        $books[$dates[$i]] = 0;
    }

    foreach ($books as $date => $bookCount) {
        $books[$date] = leadsWithCustomFieldCount($leadsWithDate, $customFieldId = getGetParams()['date_field_id'], $value = $date);
    }
    return $books;
}


// в самом начале валидировать что параметры есть
validateGetParams();

Introvert\Configuration::getDefaultConfiguration()->setApiKey('key', getConfigVars()['api_key']);

$api = new Introvert\ApiClient();

$books = getBooksCountForEachDayInMonth($api);
echo json_encode($books);
