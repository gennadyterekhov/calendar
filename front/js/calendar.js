var BOOKLIMIT = 5;
var main = async function() {
    // получить массив из пхп на беке
    let ajaxRes = await $.ajax({url: "http://localhost:8000/script.php?date_field_id=970969&st1=35175802&st2=35175805"});
    let dates = JSON.parse(ajaxRes);


    let selectableDates = [];
    Object.keys(dates).forEach((key, index, dict) => {
        if (dates[key] < BOOKLIMIT) {
            let tempDate = {date: new Date(key)};
            selectableDates.push(tempDate);
        }
    });
    

    $('#calendar1').glDatePicker(
        {
            showAlways: true,
            allowMonthSelect: true,
            allowYearSelect: true,
            selectedDate: new Date(Date.now()),
            selectableDates: selectableDates
        }
    );
}

main();
